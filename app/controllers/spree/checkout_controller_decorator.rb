Spree::CheckoutController.class_eval do

  def update
    if object_params[:line_item]
      object_params[:line_item].each do |line_item|
        line_item = line_item[1]
        li = Spree::LineItem.find line_item[:id]
        line_item.delete(:id)
        li.update_attributes(line_item)
      end
      object_params.delete :line_item
    end
    
    if @order.update_attributes(object_params)
      fire_event('spree.checkout.update')
      if @order.next
        state_callback(:after)
      else
        flash[:error] = t(:payment_processing_failed)
        respond_with(@order, :location => checkout_state_path(@order.state))
        return
      end

      if @order.state == "complete" || @order.completed?
        flash.notice = t(:order_processed_successfully)
        flash[:commerce_tracking] = "nothing special"
        flash[:conversion] = '
        <script type="text/javascript">
        var google_conversion_id = 987298221;
        var google_conversion_language = "it";
        var google_conversion_format = "2";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "serOCJOQ_RAQrfPj1gM";
        var google_conversion_value = 0;
        var google_remarketing_only = false;
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/987298221/?value=0&amp;label=serOCJOQ_RAQrfPj1gM&amp;guid=ON&amp;script=0"/>
        </div>
        </noscript>
        '
        respond_with(@order, :location => completion_route)
      else
        respond_with(@order, :location => checkout_state_path(@order.state))
      end
    else
      flash.notice = @order.errors.full_messages
      respond_with(@order) { |format| format.html { render :edit } }
    end
  end
end