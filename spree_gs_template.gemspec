# encoding: UTF-8
Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'spree_gs_template'
  s.version     = '1.1.5'
  s.summary     = 'Extension to manage GS Template'
  s.required_ruby_version = '>= 1.8.7'

  s.author            = 'Diginess'
  s.email             = 'info@diginess.com'

  s.files        = Dir['CHANGELOG', 'README.md', 'LICENSE', 'lib/**/*', 'app/**/*', 'db/**/*', 'config/**/*']
  s.require_path = 'lib'
  s.requirements << 'none'

  s.add_dependency 'spree_core', '>= 1.1.3'
	s.add_dependency 'mailchimp-api'
end
