Spree::UserRegistrationsController.class_eval do
	after_filter :set_conversion, :only => [:create]

	def set_conversion
		flash[:conversion] = '
		<script type="text/javascript">
		var google_conversion_id = 987298221;
		var google_conversion_language = "it";
		var google_conversion_format = "2";
		var google_conversion_color = "ffffff";
		var google_conversion_label = "2oqPCJuP_RAQrfPj1gM";
		var google_conversion_value = 0;
		var google_remarketing_only = false;
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/987298221/?value=0&amp;label=2oqPCJuP_RAQrfPj1gM&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
		'
	end

	def subscribe
		if (request.post? && !params[:mc_email].blank? && !params[:mc_privacy].blank?)
			mc = Mailchimp::API.new("b801af55843122b8cf11d3d1e43423ab-us10")
			list_id = '999316cfe4'

			merge_vars = {}
			# merge_vars['FNAME'] = params[:mc_name] if !params[:mc_name].blank?
			# merge_vars['CNAME'] = params[:mc_company] if !params[:mc_company].blank?

			begin
				res = mc.lists.subscribe(list_id, { 'email' => params[:mc_email].downcase }, merge_vars, nil, false, true, true, true)
				flash[:notice] = t("newsletter.success_subscribe")
			rescue Mailchimp::Error => e
				flash[:notice] = t("newsletter.error_subscribe")
			end
		end

	end
end